#include <iostream>
#include <cstdio>
#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_image.h>
using namespace std;
enum marca{L=' ', M='#', T='?', C='*'};
const int REN = 5;
const int COL = 5;
bool ruta( marca mapa[REN][COL],ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *queue);
bool ruta ( marca mapa[REN][COL], int, int,ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *queue);
void mostrar( marca mapa[REN][COL] );
void mostrar (ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *queue, marca mapa[REN][COL]);
int main(){
  if(!al_init()) {
    fprintf(stderr, "failed to initialize allegro!\n");
    return -1;
  }
  al_init_primitives_addon();
  al_init_native_dialog_addon();
  al_init_image_addon();
  ALLEGRO_EVENT_QUEUE *queue=al_create_event_queue();
  ALLEGRO_DISPLAY *display=al_create_display(800,800);
  if(!display) {
    fprintf(stderr, "failed to create display!\n");
    return -1;
  }
  marca mapa[REN][COL] = {
    {L,M,L,M,M},
    {L,M,L,M,M},
    {L,L,L,M,L},
    {L,M,L,L,L},
    {L,M,L,M,L}
  };
  mostrar(display,queue,mapa);
  mostrar (mapa);
  cout << boolalpha << ruta (mapa,display,queue) << endl;
  mostrar (mapa);
  al_destroy_display(display);
}
bool ruta( marca mapa[REN][COL],ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *queue){
  return ruta( mapa, 0, 0,display,queue);
}
bool ruta ( marca mapa[REN][COL], int r, int c, ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *queue){
  if (r < 0 || c < 0 || r >= REN || c >= COL)
    return false; // la celda esta fuera de los limites
  else if ( mapa[r][c] != L )
    return false; // la celda esta en la barrera o en el punto muerto
  else if ( r == REN -1 && c == COL -1 ){
    mapa[r][c] = C; // camino libre
    mostrar(display,queue,mapa);
    return true;
  }
  else { // caso recursivo
    // intenta hallar una trayectoria desde cada celda vacia
    mapa[r][c] = C; // arriba abajo izquierda derecha
    mostrar(display,queue,mapa);
    if ( ruta( mapa, r-1, c,display,queue) || ruta( mapa, r+1, c,display,queue) ||
	 ruta( mapa, r, c-1,display,queue) || ruta( mapa, r, c+1,display,queue) ) {
      return true;
    } else {
      mapa[r][c] = T; // punto muerto camino sin salida
      mostrar(display,queue,mapa);
      return false;
    }
  }
}
void mostrar (marca mapa[REN][COL]){
  for(int i = 0; i < REN; i++){
    for(int j = 0; j < COL; j++)
      cout << (char)mapa[i][j];
    cout << endl;
  }
  return;
}
void mostrar (ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *queue, marca mapa[REN][COL]){
  float sw,sh,dw=152,dh=152;
  ALLEGRO_COLOR color_map=al_color_name("chocolate");
  ALLEGRO_BITMAP *CAMINO=al_load_bitmap("Camino.png");
  ALLEGRO_BITMAP *MURO=al_load_bitmap("muro.jpg");
  ALLEGRO_BITMAP *LIBRE=al_load_bitmap("Libre.png");
  ALLEGRO_BITMAP *SINSALIDA=al_load_bitmap("SinSalida.jpg");
  ALLEGRO_BITMAP *casilla=NULL;
  //al_set_target_bitmap(al_get_backbuffer(display));
  al_draw_filled_rectangle(20,20,780,780,color_map);
  //al_draw_scaled_bitmap(LIBRE,0,0,al_get_bitmap_width(LIBRE),al_get_bitmap_height(LIBRE),20,20,152,152,0);
  for (int i = 1; i <= 4; i++){
    al_draw_line(20+ 152 * i, 20, 20+152 * i, 780, al_map_rgb(255, 255, 255), 3);//Estas son las lineas de las columnas
    al_draw_line(20, 20+ 152*i, 780, 20 + 152 * i, al_map_rgb(255, 255, 255), 3);//Estas son las lineas de las filas
  }
  for(int i=0; i<REN;i++){
    for(int j=0; j<COL;j++){
      //al_set_target_bitmap(al_get_backbuffer(display));
      switch (mapa[i][j]){
      case L:
	casilla=LIBRE;
	sw=al_get_bitmap_width(LIBRE);
	sh=al_get_bitmap_height(LIBRE);
	break;
      case M:
	casilla=MURO;
	sw=al_get_bitmap_width(MURO);
	sh=al_get_bitmap_height(MURO);
	break;
      case T:
	casilla=SINSALIDA;
	sw=al_get_bitmap_width(SINSALIDA);
	sh=al_get_bitmap_height(SINSALIDA);
	break;
      case C:
	casilla=CAMINO;
	sw=al_get_bitmap_width(CAMINO);
	sh=al_get_bitmap_height(CAMINO);
	break;
      }
      al_draw_scaled_bitmap(casilla,0,0,sw,sh,20+152*j,20+152*i,152,152,0);
      casilla=NULL;
    }
  }
  al_flip_display();
  al_rest(1);
}

all: main Laberinto/Laberinto

main: main.cpp
	g++ -o "main" "main.cpp" -pthread -lrt `pkg-config --libs allegro-5`

#	g++ -o "main" "main.cpp" -pthread -lrt `pkg-config --libs allegro-5 allegro_image-5 allegro_primitives-5 allegro_font-5`

Laberinto/Laberinto: Laberinto/Laberinto.cpp
	g++ -o LaberintoX Laberinto/Laberinto.cpp -pthread -lrt `pkg-config --libs allegro-5 allegro_image-5 allegro_primitives-5 allegro_dialog-5 allegro_color-5`

clean:
	rm -f Laberinto main

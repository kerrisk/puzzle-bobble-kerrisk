#include <stdio.h>
#include <allegro5/allegro.h>
#include <math.h>
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_native_dialog.h"
 
const float FPS = 60;
const int SCREEN_W = 1067;
const int SCREEN_H = 600;
const int BOUNCER_SIZE = 50;

ALLEGRO_DISPLAY *display = NULL;
ALLEGRO_EVENT_QUEUE *event_queue = NULL;
ALLEGRO_TIMER *timer = NULL;
ALLEGRO_BITMAP *bouncer = NULL;
ALLEGRO_BITMAP *canon = NULL;

int main(int argc, char **argv){

   float bouncer_x = SCREEN_W / 2.0 - BOUNCER_SIZE / 2.0;
   float bouncer_y = SCREEN_H - BOUNCER_SIZE;
   float vel=20;
   float bouncer_dx = 0.0, bouncer_dy = vel;
   bool redraw = true;
   float contador=0;
   al_init_image_addon();
 
   if(!al_init()) {
      fprintf(stderr, "failed to initialize allegro!\n");
      return -1;
   }
   if(!al_install_keyboard()) {
      fprintf(stderr, "failed to initialize the keyboard!\n");
      return -1;
   }
 
   timer = al_create_timer(1.0 / FPS);
   if(!timer) {
      fprintf(stderr, "failed to create timer!\n");
      return -1;
   }
 
   display = al_create_display(SCREEN_W, SCREEN_H);
   if(!display) {
      fprintf(stderr, "failed to create display!\n");
      al_destroy_timer(timer);
      return -1;
   }

   bouncer = al_load_bitmap("pelotita.png");
   if(!bouncer) {
      fprintf(stderr, "failed to create bouncer bitmap!\n");
      al_destroy_display(display);
      al_destroy_timer(timer);
      return -1;
   }
 if(!al_init_image_addon()) {
      al_show_native_message_box(display, "Error", "Error", "Failed to initialize al_init_image_addon!", 
                                 NULL, ALLEGRO_MESSAGEBOX_ERROR);
      return 0;
   }


   al_set_target_bitmap(bouncer);

   

   al_set_target_bitmap(al_get_backbuffer(display));
 
   event_queue = al_create_event_queue();
   if(!event_queue) {
      fprintf(stderr, "failed to create event_queue!\n");
      al_destroy_bitmap(bouncer);
      al_destroy_display(display);
      al_destroy_timer(timer);
      return -1;
   }
 
   al_register_event_source(event_queue, al_get_display_event_source(display));

   al_register_event_source(event_queue, al_get_keyboard_event_source());
 
   al_register_event_source(event_queue, al_get_timer_event_source(timer));
 
   al_clear_to_color(al_map_rgb(0,0,0));
 
   al_flip_display();
 
   al_start_timer(timer);
 
   while(1)
   {
      ALLEGRO_EVENT ev;
      al_wait_for_event(event_queue, &ev);
 
      if(ev.type == ALLEGRO_EVENT_TIMER) {
         if(contador<2) {
            bouncer_x = SCREEN_W / 2.0 - BOUNCER_SIZE / 2.0;
            bouncer_y = SCREEN_H - BOUNCER_SIZE;     
         }
         else{
            if(bouncer_x < 0 || bouncer_x > SCREEN_W - BOUNCER_SIZE) {
               bouncer_dx = -bouncer_dx;
               printf("x:%f\n", bouncer_dx );
            }

            if(bouncer_y < 0 || bouncer_y > SCREEN_H - BOUNCER_SIZE) {
               bouncer_dy = -bouncer_dy;
               printf("y:%f\n", bouncer_dy );
            }


            bouncer_x += bouncer_dx;
            bouncer_y += bouncer_dy;
            printf("dx:%f\n", bouncer_dx );
            printf("dy:%f\n", bouncer_dy );
            printf("x:%f\n", bouncer_x );
            printf("y:%f\n", bouncer_y );


            
         }
         redraw = true;
      }
      else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
         switch(ev.keyboard.keycode) {
            case ALLEGRO_KEY_SPACE:
               if(contador<3)
                  contador ++;
               else
                  contador=0;
               break;
            case ALLEGRO_KEY_RIGHT:
               if(bouncer_dx>(vel/10)*9){
                  bouncer_dx=(vel/10)*9;
                  
               }else 
               if(bouncer_dx<-(vel/10)*9){
                  bouncer_dx=-(vel/10)*9;
                  
               }
               else{
               bouncer_dx+=.1;
               }
               bouncer_dy=vel-fabs(bouncer_dx);
               printf("dx:%f\n", bouncer_dx );
               printf("dy:%f\n", bouncer_dy );
               break;
         
            case ALLEGRO_KEY_LEFT:
                if(bouncer_dx>(vel/10)*9){
                  bouncer_dx=(vel/10)*9;
                  
               }else 
               if(bouncer_dx<-(vel/10)*9){
                  bouncer_dx=-(vel/10)*9;
                  
               }
               else{
               bouncer_dx-=.1;
               }
               bouncer_dy=vel-fabs(bouncer_dx);
               printf("dx:%f\n", bouncer_dx );
               printf("dy:%f\n", bouncer_dy );
               break;
         }
         

      }
      else if(ev.type == ALLEGRO_EVENT_KEY_UP) {
         switch(ev.keyboard.keycode) {
            case ALLEGRO_KEY_SPACE:
               if(contador<3)
                  contador ++;
               else
                  contador=0;
         }
            
      }
      else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
         break;
      }
 
      if(redraw && al_is_event_queue_empty(event_queue)) {
         redraw = false;

         al_clear_to_color(al_map_rgb(0,0,0));

         al_draw_bitmap(bouncer, bouncer_x, bouncer_y, 10);


         al_flip_display();
      }
   }
 
   al_destroy_bitmap(bouncer);
   al_destroy_timer(timer);
   al_destroy_display(display);
   al_destroy_event_queue(event_queue);
 
   return 0;
}
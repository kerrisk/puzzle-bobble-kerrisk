#include <iostream>

#include "Grafo.h"

using namespace std;

int main()
{
    Grafo G;
    G.Inicializa();
    int opc;
    list<Vertice *> *color;
    G.InsertaVertice(111,1);
    G.InsertaVertice(222,3);
    G.InsertaVertice(333,1);
    G.InsertaVertice(444,1);
    G.InsertaArista(G.GetVertice(111), G.GetVertice(222));
    G.InsertaArista(G.GetVertice(222), G.GetVertice(333));
    G.InsertaArista(G.GetVertice(222), G.GetVertice(111));
    G.InsertaArista(G.GetVertice(333), G.GetVertice(222));
    G.InsertaArista(G.GetVertice(444), G.GetVertice(222));
    G.InsertaArista(G.GetVertice(222), G.GetVertice(444));
    G.InsertaArista(G.GetVertice(444), G.GetVertice(111));
    G.InsertaArista(G.GetVertice(111), G.GetVertice(444));
    G.InsertaArista(G.GetVertice(333), G.GetVertice(444));
    G.InsertaArista(G.GetVertice(444), G.GetVertice(333));






    do
    {
        system("clear");
        cout<<"1. Ingresar Vertice"<<endl;
        cout<<"2. Ingresar arista"<<endl;
        cout<<"3. Lista de adyacencia"<<endl;
        cout<<"4. Tamano"<<endl;
        cout<<"5. Eliminar vertice"<<endl;
        cout<<"6. Eliminar arista"<<endl;
        cout<<"7. Anular"<<endl;
        //cout<<"-8. Recorrido en anchura"<<endl;
        cout<<"8. Recorrido en profundidad"<<endl;
        /*cout<<"-10. Primero en anchura"<<endl;
        cout<<"-11. Primero en profundidad"<<endl;
        cout<<"-12. Primero el mejor"<<endl;*/
        cout<<"9. Recorrido por color"<<endl;
        cout<<"10. Eliminacion por Color"<<endl;
        cout<<"11. Salir"<<endl;
        cout<<endl<<"Elija una opcion: ";
        cin>>opc;
        switch(opc)
        {
        case 1:
        {
            int id;
            int color;
            system("clear");
            cout<<"Ingrese el id del vertice: ";
            cin>>id;
            cout<<"Ingrese el color del vertice: ";
            cin>>color;
            G.InsertaVertice(id,color);
            cin.get();
            break;
        }
        case 2:
        {
            int origen, destino;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el id del vertice origen: ";
                cin.ignore();
                cin>>origen;
                cout<<"Ingrese el id del vertice destino: ";
                cin>>destino;

                if(G.GetVertice(origen) == NULL || G.GetVertice(destino) == NULL)
                {
                    cout<<"Uno de los vertices no es valido"<<endl;
                }
                else
                {
                    
                    G.InsertaArista(G.GetVertice(origen), G.GetVertice(destino));
                    G.InsertaArista(G.GetVertice(destino), G.GetVertice(origen));
                }
            }
            cin.get();
            break;
        }
        case 3:
        {
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                G.ListaAdyacencia();
            }
            cin.get();
            cin.get();            
            break;
        }
        case 4:
        {
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Tamano: "<<G.Tamano()<<endl;
            }
            cin.get();
            cin.get();
            break;
        }
        case 5:
        {
            int id;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el nombre del vertice a eliminar: ";
                cin>>id;
                if(G.GetVertice(id) == NULL)
                {
                    cout<<"Vertice invalido"<<endl;
                }
                else
                {
                   G.EliminarVertice(G.GetVertice(id));
                }
            }
            cin.get();
            break;
        }
        case 6:
        {
            int origen, destino;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese del nombre del vertice origen: ";
                cin>>origen;
                cout<<"Ingrese el nombre del vertice destino: ";
                cin>>destino;
                if(G.GetVertice(origen) == NULL || G.GetVertice(destino) == NULL)
                {
                    cout<<"Vertices no validos"<<endl;
                }
                else
                {
                    G.EliminarArista(G.GetVertice(origen), G.GetVertice(destino));
                }
            }
            cin.get();
            break;
        }
        case 7:
        {
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
               G.Anular();
            }
            cin.get();
            break;
        }
        /*case 8:
        {
            string nombre;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el nombre del vertice inicial: ";
                cin.ignore();
                getline(cin, nombre, '\n');
                if(G.GetVertice(nombre) == NULL)
                {
                    cout<<"Ese vertice es invalido"<<endl;
                }
                else
                {
                    G.RecorridoAnchura(G.GetVertice(nombre));
                }
            }
            cin.get();
            break;
        }*/
        case 8:
        {
            int id;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el id del vertice inicial: ";
                cin>>id;
                if(G.GetVertice(id) == NULL)
                {
                    cout<<"Ese vertice es invalido"<<endl;
                }
                else
                {
                    G.ListaAdyacencia();
                    cout<<endl;
                    G.RecorridoProfundidad(G.GetVertice(id));

                }
            }
            cin.get();
            cin.get();
            break;
        }
        /*case 10:
        {
            string origen, destino;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el nombre del vertice origen: ";
                cin.ignore();
                getline(cin, origen, '\n');
                cout<<"Ingrese el nombre del vertice destino: ";
                getline(cin, destino, '\n');
                if(G.GetVertice(origen) == NULL || G.GetVertice(destino) == NULL)
                {
                    cout<<"Vertices invalidos"<<endl;
                }
                else
                {
                    G.PrimeroAnchura(G.GetVertice(origen), G.GetVertice(destino));
                }
            }
            cin.get();
            break;
        }
        case 11:
        {
            string origen, destino;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el nombre del vertice origen: ";
                cin.ignore();
                getline(cin, origen, '\n');
                cout<<"Ingrese el nombre del vertice destino: ";
                getline(cin, destino, '\n');
                if(G.GetVertice(origen) == NULL || G.GetVertice(destino) == NULL)
                {
                    cout<<"Vertices invalidos"<<endl;
                }
                else
                {
                    G.PrimeroProfundidad(G.GetVertice(origen), G.GetVertice(destino));
                }
            }
            cin.get();
            break;
        }
        case 12:
        {
            string origen, destino;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el nombre del vertice origen: ";
                cin.ignore();
                getline(cin, origen, '\n');
                cout<<"Ingrese el nombre del vertice destino: ";
                getline(cin, destino, '\n');
                if(G.GetVertice(origen) == NULL || G.GetVertice(destino) == NULL)
                {
                    cout<<"Vertices invalidos"<<endl;
                }
                else
                {
                    G.PrimeroMejor(G.GetVertice(origen), G.GetVertice(destino));
                }
            }
            cin.get();
            break;
        }*/

         case 9:
        {
            int id;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el id del vertice inicial: ";
                cin>>id;
                if(G.GetVertice(id) == NULL)
                {
                    cout<<"Ese vertice es invalido"<<endl;
                }
                else
                {
                    G.ListaAdyacencia();
                    cout<<endl;
		    color=G.RecorridoPorColor(G.GetVertice(id));
		    cout<<color->size();
                }
            }
            cin.get();
            cin.get();
            break;
        }
        case 10:
        {
            int id;
            Vertice *aux;
            system("clear");
            if(G.Vacio())
            {
                cout<<"El grafo esta vacio"<<endl;
            }
            else
            {
                cout<<"Ingrese el id del vertice inicial: ";
                cin>>id;
                if(G.GetVertice(id) == NULL)
                {
                    cout<<"Ese vertice es invalido"<<endl;
                }
                else
                {
                    G.ListaAdyacencia();
                    cout<<endl;
                    color=G.RecorridoPorColor(G.GetVertice(id));
                    if(color->size()>=3){
                        while(!color->empty()){
                            aux=color->back();
                            color->pop_back();
                            G.EliminarVertice(aux);
                        }
                    }
                cout<<endl<<endl;
                G.ListaAdyacencia();

                }
            }
            cin.get();
            cin.get();
            break;
        }

        case 11:
        {
            break;
        }
        default:
        {
            cout<<"Elija una opcion valida"<<endl;
        }
        }
    }
    while(opc != 14);
    return 0;
}

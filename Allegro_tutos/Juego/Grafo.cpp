#include "Grafo.h"
#include <string>
#include <cstdlib>

void Grafo::Inicializa(){

	h=NULL;
}

bool Grafo::Vacio(){
	if(h==NULL){
		return true;
	}else 
	return false;
}

int Grafo::Tamano(){
	int cont=0;
	Vertice *aux;
	aux=h;
	while(aux!=NULL){
		cont++;
		aux=aux->sig;
	}
	return cont;
}

Vertice *Grafo::GetVertice(int id){
	Vertice *aux;
	aux=h;
	while(aux!=NULL){
		if(aux->id==id){
			return aux;
		}
		aux=aux->sig;
	}
	return NULL;
}

void Grafo::InsertaVertice(int id, int color){
	Vertice *nuevo= new Vertice;
	nuevo->id=id;
	nuevo->color=color;
	nuevo->sig=NULL;
	nuevo->ady=NULL;
	if(Vacio()){
		h=nuevo;
	}
	else{
		Vertice *aux;
		aux=h;
		while(aux->sig!=NULL){
		aux=aux->sig;
		}
		aux->sig=nuevo;
	}
}

void Grafo::InsertaArista(Vertice *origen, Vertice *destino){
	Arista *nueva= new Arista;
	nueva->sig=NULL;
	nueva->ady=NULL;

	Arista *aux;

	aux = origen->ady;

	if(aux==NULL){					
		nueva->ady=destino;
		origen->ady=nueva;
		
	}
	else{
		while(aux->sig!=NULL){
		aux=aux->sig;
		}
		aux->sig=nueva;
		nueva->ady=destino;

	}

}

void Grafo::ListaAdyacencia(){
	Vertice *VertAux;
	Arista *ArisAux;
	VertAux=h;
	while(VertAux!=NULL){
		cout<<VertAux->id<<"_"<<VertAux->color<<"->";
		ArisAux=VertAux->ady;
		while(ArisAux!=NULL){
			cout<<ArisAux->ady->id<<"_"<<ArisAux->ady->color<<"->";

			ArisAux=ArisAux->sig;
		}
		cout<<endl;
		VertAux=VertAux->sig;
	}
}

void Grafo::Anular(){
	Vertice *aux;

	while(h != NULL){
		aux=h;
		h=h->sig;
		delete(aux);
	}
}

void Grafo::EliminarArista(Vertice *origen, Vertice *destino){
	Arista *actual;
	Arista *anterior;
	bool band=false;

	actual = origen->ady;
	if(actual==NULL){
		printf("El vertice origen no tiene aristas\n");
	}
	else 
	if(actual->ady==destino)
	{
		origen->ady=actual->sig;
		delete(actual);
	}
	else{
		while(actual != NULL){
			if(actual->ady==destino){
				band=true;
				anterior->sig=actual->sig;
				delete(actual);
				break;
			}
			anterior = actual;
			actual=actual->sig;

		}
		if(band==false){
			cout<<"Esos dos vertices no estan conectados"<<endl;
		}
	}
}


void Grafo::EliminarVertice(Vertice *vert){
	Vertice *actual, *anterior;
	Arista *aux;

	actual=h;

	while(actual != NULL){
		aux=actual->ady;
		while(aux!=NULL){
			if(aux->ady==vert){
				EliminarArista(actual, aux->ady);
				break;
			}
			aux=aux->sig;
			
		}
		actual=actual->sig;
	}

	actual=h;

	if(h==vert){
		h=h->sig;
		delete(actual);
	}
	else{
		while(actual!=vert){
			anterior=actual;
			actual=actual->sig;
		}
		anterior->sig=actual->sig;
		delete(actual);
	}
}
int Grafo::GetColor(Vertice *vert){
	return vert->color;
}
int Grafo::GetId(Vertice *vert){
	return vert->id;
}
list<Vertice *> *  Grafo::RecorridoProfundidad(Vertice *origen){
	int band, band2;
	Vertice *actual;
	stack<Vertice *> pila;
	list<Vertice *> lista;
	list<Vertice *> *color=new list<Vertice *>;
	list<Vertice *>::iterator i;
	pila.push(origen);

	while(!pila.empty()){
		band=0;
		actual = pila.top();
		pila.pop();

		for(i=lista.begin() ;i!=lista.end(); i++){
			if(*i== actual){
				band=1;
			}

		}
		if(band==0){
			cout<<actual->id<<"_"<<actual->color<<", ";
			lista.push_back(actual);
			color->push_back(actual);

			Arista *aux;
			aux=actual->ady;

			while(aux!=NULL){
				band2=0;
				for(i=lista.begin(); i!=lista.end(); i++){
					if(*i== aux->ady){
						band2=1;
					}
				}
				if(band2==0){
					pila.push(aux->ady);
				}
				aux=aux->sig;
			}
		}
	}
	return color;

}
list<Vertice *> * Grafo::RecorridoPorColor(Vertice *origen){
	int band, band2;
	Vertice *actual;
	stack<Vertice *> pila;
	list<Vertice *> lista;
	list<Vertice *> *color=new list<Vertice *>;
	list<Vertice *>::iterator i;
	pila.push(origen);

	while(!pila.empty()){
		band=0;
		actual = pila.top();
		pila.pop();

		for(i=lista.begin() ;i!=lista.end(); i++){
			if(*i== actual){
				band=1;
			}

		}
		if(band==0 && actual->color == origen->color){
			cout<<actual->id<<"_"<<actual->color<<", ";
			lista.push_back(actual);
			color->push_back(actual);

			Arista *aux;
			aux=actual->ady;

			while(aux!=NULL){
				band2=0;
				for(i=lista.begin(); i!=lista.end(); i++){
					if(*i== aux->ady){
						band2=1;
					}
				}
				if(band2==0){
					pila.push(aux->ady);
				}
				aux=aux->sig;
			}
		}
	}
	return color;
}

void Grafo::SetColor(Vertice *vert, int color){
	vert->color=color;
}


/*void Grafo::Verifica(Vertice *vert){
	color=vert->color;
	cont=0;
	Arista *Aaux;
	Vertice *Vaux;
	aux=vert;

	while(Aaux!=nul)



}*/

























#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <allegro5/allegro.h>
#include <math.h>
#include <ctime>
#include <list>
#include <string>
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_native_dialog.h"
#include "allegro5/allegro_font.h"
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "Grafo.h"
#include <fstream>


   

const float FPS = 60;
const int SCREEN_W = 260;
const int SCREEN_H = 455;
const int BOUNCER_SIZE = 50;
const int CANON_SIZE = 100;

struct Jugador{
  char alias[100];
  double puntaje;
};
fstream Info;
FILE * Archivo;
ALLEGRO_DISPLAY *display = NULL;
ALLEGRO_EVENT_QUEUE *event_queue = NULL;
ALLEGRO_TIMER *timer = NULL;
ALLEGRO_BITMAP *fondo = NULL;
ALLEGRO_BITMAP *bouncer = NULL;
ALLEGRO_BITMAP *bouncer_roja = NULL;
ALLEGRO_BITMAP *bouncer_verde = NULL;
ALLEGRO_BITMAP *bouncer_morada = NULL;
ALLEGRO_BITMAP *bouncer_cafe = NULL;
ALLEGRO_BITMAP *bouncer_azul = NULL;
ALLEGRO_BITMAP *bouncer2 = NULL;
ALLEGRO_BITMAP *canon = NULL;
ALLEGRO_BITMAP *inicio = NULL;
ALLEGRO_BITMAP *Techo = NULL;
ALLEGRO_SAMPLE *burble=NULL;
ALLEGRO_SAMPLE *tone=NULL;

int NIVEL_ACTUAL=1, NIVEL_SIG=1, **mat,shotsfired=0,techo=0,lost=0, pos=0, sonido=0;
double puntaje=0,nuevo_puntaje=0,tiempoDisparoAuto=10,tiempo=0,shot=0,ant_puntaje=0;
char Alias[100];
Grafo *G;

void Verificar(int *, int *,  int *, float , float , int *, int*,list<Vertice*> *lista);
void Dibujar_matriz(float,float);
void Crear(int *);
void Dibujar_pelota(int, int,int , int);
void Dibujar_color(int);
void Crear_bitmaps();
void Destruir();
void Conectar(int *, int *,  int,  int);
void Verificar_Eliminacion(int id, list<Vertice*> *lista);
void EliminarP_Matriz(int id);
void Verificar_Techo(list<Vertice*> *lista);
void Verificar_Matriz();
void Cascaron();
void Guardar();
void Consulta();
void Sort();
void Imprimir();

//void Precargar_Matriz(string nombre, int **mat, Grafo *G);
#include "Niveles.h"
int main(int argc, char **argv){
  srand(time(NULL));
  char buf[1000];
  strcpy(Alias,"No name\0");
  Nivel *nivel;
  Nivel1 *nivel1;
  Nivel2 *nivel2;
  Nivel3 *nivel3;
  G=new Grafo;
  list<Vertice*> *lista;
  G->Inicializa();
  G->InsertaVertice(999,0);
  mat= (int **) calloc(14, sizeof(int *));
  for(int i=0; i<14 ; i++){
    mat[i]=(int*) calloc(8, sizeof(int*));
  }
  for(int i=0; i<14 ; i++){
    for(int j=0; j<8 ;j++){
      mat[j][i]=0;
    }
  }
  if(argc>1){
    if(!strcmp(argv[1],"1")){
      nivel=new Nivel1;
      NIVEL_ACTUAL=1;
      NIVEL_SIG=1;
    }
    else if(!strcmp(argv[1],"2")){
      nivel=new Nivel2;
      NIVEL_ACTUAL=2;
      NIVEL_SIG=2;
    }
    else if(!strcmp(argv[1],"3")){
      nivel=new Nivel3;
      NIVEL_ACTUAL=3;
      NIVEL_SIG=3;
    }
    else {
      nivel=new Nivel1;
      NIVEL_ACTUAL=1;
      NIVEL_SIG=1;
    }
  }
  else{
      nivel=new Nivel1;
  }
  double canon_angle;
  float bouncer_x = SCREEN_W / 2.0 - BOUNCER_SIZE / 2.0;
  float bouncer_y = SCREEN_H - BOUNCER_SIZE;
  float vel=20;
  float bouncer_dx = 0.0, bouncer_dy = vel;
  float dir_dx = 0.0, dir_dy = vel;
  bool redraw = true,viajando=false;
  int salir=0;
  int ant_j[]={0},ant_i[]={0},color[]={0},encontrado[]={0};
  int contador[]={0};
  bool key_right=false;
  bool key_left = false;
  //Nivel1(tiempoDisparoAuto,);
  /*============================================================================*/
  /*=========================  INICIALIZACION DE ALLEGRO   =====================*/
  /*============================================================================*/
  if(!al_init()) {
    fprintf(stderr, "failed to initialize allegro!\n");
    return -1;
  }
  if(!al_install_keyboard()) {
    fprintf(stderr, "failed to initialize the keyboard!\n");
    return -1;
  }

  if(!al_init_native_dialog_addon()) {
    fprintf(stderr, "failed to initialize native dialog addon!\n");
    al_destroy_timer(timer);
    return -1;
  }
  timer = al_create_timer(1.0 / FPS);
    
  if(!timer) {
    fprintf(stderr, "failed to create timer!\n");
    return -1;
  }
   
  display = al_create_display(600, SCREEN_H);
  if(!display) {
    fprintf(stderr, "failed to create display!\n");
    al_destroy_timer(timer);
    return -1;
  }
   
  if(!al_init_image_addon()) {
    fprintf(stderr, "failed to initialize image addon!\n");
    return 0;
  }
    
  if(!al_init_font_addon()) {
    fprintf(stderr, "failed to initialize native dialog addon!\n");
    al_destroy_timer(timer);
    return -1;
  }

  if(!al_init_ttf_addon()) {
    fprintf(stderr, "failed to initialize ttf addon!\n");
    al_destroy_timer(timer);
    return -1;
  }
  if(!al_install_audio()){
      fprintf(stderr, "failed to initialize audio!\n");
      return -1;
   }

   if(!al_init_acodec_addon()){
      fprintf(stderr, "failed to initialize audio codecs!\n");
      return -1;
   }
  
   if (!al_reserve_samples(1)){
      fprintf(stderr, "failed to reserve samples!\n");
      return -1;
   }
  
   burble = al_load_sample( "cartoon015.wav" );

   if (!burble){
      printf( "Audio clip sample not loaded (burble)!\n" ); 
      return -1;
   }
   tone = al_load_sample( "Fondo.wav" );

   if (!tone){
      printf( "Audio clip sample not loaded (tone)!\n" ); 
      return -1;
   }
   if (!al_init_primitives_addon()){
       printf("failed to initialize primitives addon!\n");
       return -1;
   }


  nivel->Cargar();
   
  Cascaron();
  
  /*============================================================================*/
  /*=======================  FIN INICIALIZACION DE ALLEGRO  ====================*/
  /*============================================================================*/
  Crear_bitmaps();
  Crear(color);
  canon = al_load_bitmap("canon.png");
  float w = al_get_bitmap_width(canon);           //Dimensiones del cañon
  float h = al_get_bitmap_height(canon);          //Dimensiones del cañon
  fondo = al_load_bitmap("FONDO_JUEGO.jpg");
  inicio = al_load_bitmap("INICIO.jpg");
  Techo = al_load_bitmap("Techo.jpg");
  if(!fondo) {
    fprintf(stderr, "failed to create fondo bitmap!\n");
    Destruir();
    return -1;
  }
  if(!canon) {
    fprintf(stderr, "failed to canon bitmap!\n");
    Destruir();
    return -1;
  }


  al_set_target_bitmap(bouncer);
  al_set_target_bitmap(canon);
  al_set_target_bitmap(al_get_backbuffer(display));
  event_queue = al_create_event_queue();
  if(!event_queue) {
    fprintf(stderr, "failed to create event_queue!\n");
    Destruir();
    return -1;
  }
  ALLEGRO_FONT *letra_g=al_load_ttf_font("Font.TTF",30,0);
  ALLEGRO_FONT *letra_m=al_load_ttf_font("Font.TTF",25,0);
  ALLEGRO_FONT *letra_s=al_load_ttf_font("Font.TTF",20,0);
  if(!letra_g || !letra_m || !letra_s){
    fprintf(stderr, "failed to create fonts!\n");
    Destruir();
    return -1;
  }
  al_register_event_source(event_queue, al_get_display_event_source(display));

  al_register_event_source(event_queue, al_get_keyboard_event_source());
   
  al_register_event_source(event_queue, al_get_timer_event_source(timer));
    
  al_clear_to_color(al_map_rgb(0,0,0));
  al_draw_bitmap(inicio,0,0,0);
  al_flip_display();

  switch(al_show_native_message_box(display,"Iniciando", "Ingresa tu Alias en la terminal ", "Al terminar presiona enter. Presiona continuar para poder ingresar tu Alias en la terminal","Sin Nombre|Continuar|Salir|Highscores", ALLEGRO_MESSAGEBOX_OK_CANCEL)){
  case 1:
    strcpy(Alias,"No name\0");
    break;
  case 2:
    cout<<"Ingresa tu Alias: ";
    cin.getline(Alias,100);
    cout<<"Regresa a la pantalla de juego para iniciar....\n";
    break;
  case 3:
    Destruir();
    exit(0);
  case 4:
    Sort();
    break;
  }
  
  int start=0;
  while(!start){
    ALLEGRO_EVENT ev;
    al_wait_for_event(event_queue, &ev);
    if(ev.type== ALLEGRO_EVENT_KEY_DOWN) {
  	switch(ev.keyboard.keycode) {
	case (ALLEGRO_KEY_ENTER):
	  start=1;
	  break;
	case (ALLEGRO_KEY_ESCAPE):
	  Destruir();
	  exit(0);
	}
    }
  }
  Consulta();
  al_start_timer(timer);
  while(!salir)
    {
      ALLEGRO_EVENT ev;
      al_wait_for_event(event_queue, &ev);
      if(ev.type == ALLEGRO_EVENT_TIMER) {
	if(lost){
	  puntaje=ant_puntaje;
	  Guardar();
	  snprintf(buf,1000,"Perdiste!    Alias: %s  Puntaje= %.0f",Alias,puntaje);
	  switch(al_show_native_message_box(display,"Juego Finalizado", "", buf,"Intentar de Nuevo|Salir", ALLEGRO_MESSAGEBOX_OK_CANCEL)){
	  case 1:
	    NIVEL_ACTUAL--;
	    lost=0;
	    techo=0;
	    tiempo=0;
	    shot=0;
	    shotsfired=0;
	    Consulta();
	    break;
	  case 2:
	    Destruir();
	    salir=1;
	    exit(-1);
	    break;
	  }

	}
	/*==============================================================================================================*/
	if(NIVEL_ACTUAL!=NIVEL_SIG){
	  Guardar();
	  techo=0;
	  shotsfired=0;
	  ant_puntaje=puntaje;
	  if(NIVEL_SIG==4){
	    snprintf(buf,1000,"Ganaste!    Alias: %s  Puntaje= %.0f",Alias,puntaje);
	    switch(al_show_native_message_box(display,"Juego Finalizado", "", buf,"Reiniciar|Salir", ALLEGRO_MESSAGEBOX_OK_CANCEL)){
	    case 1:
        Consulta();
	      NIVEL_ACTUAL=0;
	      NIVEL_SIG=1;
	      break;
	    case 2:
	      Destruir();
	      salir=1;
	      exit(-1);
	      break;
	    }
	  }
	  if (typeid(*nivel)==typeid(Nivel1)){
	    nivel1=dynamic_cast<Nivel1 *>(nivel);
	    delete nivel1;
	  }else if (typeid(*nivel)==typeid(Nivel2)){
	      nivel2=dynamic_cast<Nivel2 *>(nivel);
	      delete nivel2;
	  }else if (typeid(*nivel)==typeid(Nivel3)){
	    nivel3=dynamic_cast<Nivel3 *>(nivel);
	    delete nivel3;
	  }

	  NIVEL_ACTUAL=NIVEL_SIG;
	  switch(NIVEL_ACTUAL){
	  case 1:
	    nivel=new Nivel1;
	    break;
	  case 2:
	    nivel=new Nivel2;
	    break;
	  case 3:
	    nivel=new Nivel3;
	    break;
	  }
	  nivel->Cargar();//Carga el nivel haciendo uso de polimorfismo y funciones virtuales
	}
/*==============================================================================================================*/
	if(*encontrado){
	  *encontrado=0;
	  Crear(color);
	  shotsfired++;
	}
	if(shotsfired>=8){
	  techo++;
	  shotsfired=0;
	}
  	tiempo+=(double)1/60;
  	shot+=(double)1/60;
  	
  	if(*contador<2) {               //Vuelve a la posicion original Bounce cuando presionas de nuevo space                               
  	  bouncer_x = SCREEN_W / 2.0 - BOUNCER_SIZE / 2.0;
  	  bouncer_y = SCREEN_H - BOUNCER_SIZE; 
  	  bouncer_dy=dir_dy;
  	  bouncer_dx=dir_dx;
         
  	  if(key_right==true){          //Si la tecla derecha sigue presionada
  	    if(dir_dx>(vel/10)*9){
  	      dir_dx=(vel/10)*9;
  	    }
  	    else 
  	      if(dir_dx<-(vel/10)*9){
  		dir_dx=-(vel/10)*9;
  	      }
  	      else{
  		dir_dx+=.1;
  	      }
  	    dir_dy=vel-fabs(dir_dx);
  	    printf("dx:%f\n", dir_dx );
  	    printf("dy:%f\n", dir_dy );
  	    bouncer_dy=dir_dy;
  	    bouncer_dx=dir_dx;
  	    
  	    canon_angle=atan(bouncer_dy/bouncer_dx);//Calcula el angulo con la tangente inversa del triangulo rectangulo que se forma con las dx dy siguientes

  	    if (canon_angle<0)
  	      canon_angle+=ALLEGRO_PI;
  	    canon_angle-=ALLEGRO_PI/2;
  	    canon_angle*=-1;
  	    printf("angle: %lfrad = %lf°\n",canon_angle,canon_angle*(180/ALLEGRO_PI));
  	  }
  	  if(key_left==true){            //Si la tecla izquierda sigue presionada
                  
  	    if(dir_dx>(vel/10)*9){
  	      dir_dx=(vel/10)*9;
                       
  	    }else 
  	      if(dir_dx<-(vel/10)*9){
  		dir_dx=-(vel/10)*9;
                       
  	      }
  	      else{
  		dir_dx-=.1;
  	      }
  	    dir_dy=vel-fabs(dir_dx);
  	    printf("dx:%f\n", dir_dx );
  	    printf("dy:%f\n", dir_dy );
  	    bouncer_dy=dir_dy;
  	    bouncer_dx=dir_dx;
  	    canon_angle=atan(bouncer_dy/bouncer_dx);//Calcula el angulo con la tangente inversa del triangulo rectangulo que se forma con las dx dy siguientes

  	    if (canon_angle<0)
  	      canon_angle+=ALLEGRO_PI;
  	    canon_angle-=ALLEGRO_PI/2;
  	    canon_angle*=-1;
  	    printf("angle: %lfrad = %lf°\n",canon_angle,canon_angle*(180/ALLEGRO_PI));
  	    
  	  }
  	  if(shot>=tiempoDisparoAuto)
  	    *contador=2;
  	}
  	else{
  	  viajando=true;
  	  shot=0;
  	  if(bouncer_x < 0 || bouncer_x > SCREEN_W - BOUNCER_SIZE) {
  	    bouncer_dx = -bouncer_dx;
  	    printf("x:%f\n", bouncer_dx );
  	  }
	  if(bouncer_y < 0 || bouncer_y > SCREEN_H - BOUNCER_SIZE){
	    bouncer_dy = -bouncer_dy;
	    printf("y:%f\n", bouncer_dy );
	  }
	  bouncer_x += bouncer_dx;
	  bouncer_y += bouncer_dy;
	  printf("dx:%f\n", bouncer_dx );
	  printf("dy:%f\n", bouncer_dy );
	  printf("x:%f\n", bouncer_x );
	  printf("y:%f\n", bouncer_y );
	  
	}
  	redraw = true;
      }
      else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
  	switch(ev.keyboard.keycode) {
  	case ALLEGRO_KEY_SPACE:
  	  if(*contador<3)                  
  	    *contador+=1;
  	  else if(!viajando)
  	    *contador=0;
  	  break;
  	case ALLEGRO_KEY_RIGHT:
  	  key_right=true;
  	  break;
           
  	case ALLEGRO_KEY_LEFT:
  	  key_left=true;
  	  break;
  	case ALLEGRO_KEY_ESCAPE:
  	  salir=1;
  	  break;
  	}
      }
      else if(ev.type == ALLEGRO_EVENT_KEY_UP){
  	switch(ev.keyboard.keycode) {
  	case ALLEGRO_KEY_SPACE:
  	  if(*contador<3)
  	    *contador+=1;
  	  else if(!viajando)
  	    *contador=0;
  	  break;
  	case ALLEGRO_KEY_RIGHT:
  	  key_right=false;
  	  break;
  	case ALLEGRO_KEY_LEFT:
  	  key_left=false;
  	  break;
  	}
      }
      else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
  	salir=1;
  	break;
      }
      if(redraw && al_is_event_queue_empty(event_queue)) {
  	redraw = false;
  	al_draw_bitmap(fondo,0,0,0);
	al_draw_line(0,380,SCREEN_W,380,al_color_name("red"),3);
  	al_draw_rotated_bitmap(canon,25,100,125,425,canon_angle, 0);
  	
  	//al_draw_bitmap(canon, 100, 340, 1);
  	//Verficamos que no haya pelotas donde vamos recorriendo
  	Verificar(ant_j,ant_i,contador, bouncer_y, bouncer_x,  color, encontrado, lista);
  	if (*encontrado){
	  //Crear(color);
  	  viajando=false;
  	}
	
  	if(*encontrado==0){
  	  al_draw_bitmap(bouncer, bouncer_x,bouncer_y, 0);
  	}
    al_draw_bitmap(Techo, 0,techo*(SCREEN_H/14)-SCREEN_H, 0);
	Verificar_Matriz();
	Dibujar_matriz(bouncer_x, bouncer_y);
	puntaje+=nuevo_puntaje;
	nuevo_puntaje=0;
  if(sonido==0){
  //al_play_sample(tone, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);
  }
  	al_draw_textf(letra_g,al_map_rgb(0,0,0),265,150,0,"%s",Alias);
  	al_draw_textf(letra_s,al_map_rgb(0,0,0),580,150,ALLEGRO_ALIGN_RIGHT,"%.0f seg",tiempo);
  	al_draw_textf(letra_s,al_map_rgb(0,0,0),580,180,ALLEGRO_ALIGN_RIGHT,"Puntaje: %.0f",puntaje);
  	al_flip_display();
      }
    }
  Destruir();
  /*  for(int i=0; i<14 ; i++){
    free(mat[i]);
  }
  free(mat);*/
  return 0;

}

void Verificar(int *ant_j,int *ant_i, int *contador,float bouncer_y,float bouncer_x, int*color, int* encontrado,list<Vertice*> *lista){
  //Verifica que donde nos encontremos no se encuentre ya una pelota,
  //guardamos la posicion anterior a la que estabamos con ant_j y ant_i para en caso de que se encuentre una pelota
  //marcar la posicion anterior como ocupada 
  for(int j=0; j<14 ; j++){      //Inicio de Recorrido por toda la matriz

    if(j%2!=0){//Para las filas impares (1 , 3, ..) el numero de columnas es 7
      for(int i=0; i<8 ; i++){
	if(bouncer_y<((SCREEN_H/14)*(j+1+techo))&&bouncer_y>((SCREEN_H/14)*(j+techo))&&bouncer_x< ((SCREEN_W/8)*(i+1))&&bouncer_x>((SCREEN_W/8)*i)&&mat[j][i]==0){
	  *ant_j=j;
	  *ant_i=i;
	  if(*ant_j==0&&mat[*ant_j][*ant_i]==0){
	    mat[*ant_j][*ant_i]=1;
	    G->InsertaVertice((*ant_i)+(8*(*ant_j)),*color);
	    Conectar(ant_i,ant_j, (*ant_i)+(8*(*ant_j)), 0);
	    Verificar_Eliminacion((*ant_i)+(8*(*ant_j)), lista);
	    *contador=0;
	    *encontrado=1;
	    printf("ant_i=%i\nant_j=%i\n",  *ant_i, *ant_j);
	    break;
	  }
        	  
	}
	else if(bouncer_y<((SCREEN_H/14)*(j+1+techo))&&bouncer_y>((SCREEN_H/14)*(j+techo))&&bouncer_x< ((SCREEN_W/8)*(i+1))&&bouncer_x>((SCREEN_W/8)*i)&&mat[j][i]!=0){
	  mat[*ant_j][*ant_i]=1;
	  G->InsertaVertice((*ant_i)+(8*(*ant_j)),*color);
	  Conectar(ant_i,ant_j, (*ant_i)+(8*(*ant_j)),  0);
	  Verificar_Eliminacion((*ant_i)+(8*(*ant_j)), lista);
	  printf("Encontrado\n");
	  printf("ant_i=%i\nant_j=%i\n",  *ant_i, *ant_j);                  
	  *contador=0;
	  *encontrado=1;
	  break;
	}
      }

    }//Fin impar
    else if(j%2==0){ //Filas pares numero de columnas es 8
      for(int i=0; i<8 ;i++){
	if(bouncer_y<((SCREEN_H/14)*(j+1+techo))&&bouncer_y>((SCREEN_H/14)*(j+techo))&&bouncer_x< ((SCREEN_W/8)*(i+1))&&bouncer_x>((SCREEN_W/8)*i)&&mat[j][i]==0){
	  *ant_j=j;
	  *ant_i=i;
	  if(*ant_j==0&&mat[*ant_j][*ant_i]==0){
	    mat[*ant_j][*ant_i]=1;
	    G->InsertaVertice((*ant_i)+(8*(*ant_j)),*color);
	    Conectar(ant_i,ant_j,  (*ant_i)+(8*(*ant_j)),  1);
	    Verificar_Eliminacion((*ant_i)+(8*(*ant_j)), lista);
	    *contador=0;
	    *encontrado=1;
	    printf("ant_i=%i\nant_j=%i\n",  *ant_i, *ant_j);
	    break;
	  }
        

	}
	else if(bouncer_y<((SCREEN_H/14)*(j+1+techo))&&bouncer_y>((SCREEN_H/14)*(j+techo))&&bouncer_x< ((SCREEN_W/8)*(i+1))&&bouncer_x>((SCREEN_W/8)*i)&&mat[j][i]!=0){
	  mat[*ant_j][*ant_i]=1;
	  G->InsertaVertice((*ant_i)+(8*(*ant_j)),*color);
	  Conectar(ant_i,ant_j, (*ant_i)+(8*(*ant_j)),  1);
	  Verificar_Eliminacion((*ant_i)+(8*(*ant_j)), lista);
	  printf("Encontrado\n");
	  printf("ant_i=%i\nant_j=%i\n",  *ant_i, *ant_j);
	  *contador=0;
	  *encontrado=1;
	  break;
	}
      }
    }
  }
  if(*encontrado!=1){
    *encontrado=0;
  }
   
          
    
}


void Dibujar_matriz(float bouncer_x, float bouncer_y){
  for(int j=0; j<14; j++){
    if(j%2!=0){
      for(int i=0; i<7; i++){
  	if(mat[j][i]!=0){
  	  Dibujar_pelota((i+(8*j)),0, i ,j);
  	}
      }
    }
    else if(j%2==0){
      for(int i=0; i<8; i++){
	if(mat[j][i]!=0){
	  Dibujar_pelota((i+(8*j)),1 ,i ,j);
	}
      }
    }
      
  }
}

void Crear(int *color){
  int disponibles[]={0,0,0,0,0,0};
  list<Vertice*> *lista;
  list<Vertice *>::iterator i;
  cout<<"RecorridoProfunidad para generar color:"<<endl;
  lista=G->RecorridoProfundidad(G->GetVertice(999));
  cout<<endl;
  for (i=lista->begin(); i!=lista->end();i++){
    switch(G->GetColor(*i)){
    case 1:
      disponibles[1]=1;
      break;
    case 2:
      disponibles[2]=1;
      break;
    case 3:
      disponibles[3]=1;
      break;
    case 4:
      disponibles[4]=1;
      break;
    case 5:
      disponibles[5]=1;
      break;

    }
  }
  do{
  *color=1+rand()%5;
  printf("Color generado: %i\n",*color);
  }while(!disponibles[*color]);
  switch(*color){
  case 1:
    bouncer = bouncer_azul;
    break;
  case 2:
    bouncer = bouncer_cafe;
    break;
  case 3:
    bouncer = bouncer_morada;
    break;
  case 4:
    bouncer = bouncer_roja;
    break;
  case 5:
    bouncer = bouncer_verde;
    break;
  }
  delete lista;
}

void Dibujar_pelota(int id, int t, int i, int j){
  int color;
  //Obtenemos el color del objeto Vertice
  color=G->GetColor(G->GetVertice(id));
  Dibujar_color(color);
  if(t==0){
    
    al_draw_bitmap(bouncer2, ((SCREEN_W/8)/2)+(SCREEN_W/8)*i,(SCREEN_H/14)*j+(SCREEN_H/14)*techo, 10);
  }
  else if(t==1){
    al_draw_bitmap(bouncer2, (SCREEN_W/8)*i,(SCREEN_H/14)*j+(SCREEN_H/14)*techo, 10);
  }
}

void Dibujar_color(int color){
  switch(color){
  case 1:
    bouncer2 = bouncer_azul;
    break;
  case 2:
    bouncer2 = bouncer_cafe;
    break;
  case 3:
    bouncer2 = bouncer_morada;
    break;
  case 4:
    bouncer2 = bouncer_roja;
    break;
  case 5:
    bouncer2 = bouncer_verde;
    break;

  }
}

void Crear_bitmaps(){
   
  bouncer_azul=al_load_bitmap("pelota_azul.png");
  bouncer_verde=al_load_bitmap("pelota_verde.png");
  bouncer_cafe=al_load_bitmap("pelota_cafe.png");
  bouncer_morada=al_load_bitmap("pelota_morada.png");
  bouncer_roja=al_load_bitmap("pelota_roja.png");
  if(!bouncer_azul||!bouncer_verde||!bouncer_cafe||!bouncer_morada||!bouncer_roja) {
    fprintf(stderr, "failed to create boucer's bitmap!\n");
    Destruir();
    exit(-1);
  }

}
void Destruir(){
  //Guardar();
  al_destroy_bitmap(canon);
  al_destroy_bitmap(bouncer_roja);
  al_destroy_bitmap(bouncer_cafe);
  al_destroy_bitmap(bouncer_morada);
  al_destroy_bitmap(bouncer_azul);
  al_destroy_bitmap(bouncer_verde);
  al_destroy_bitmap(fondo);
  al_destroy_timer(timer);
  al_destroy_display(display);
  al_destroy_event_queue(event_queue);
}

void Conectar(int *ant_i, int *ant_j, int id, int par){ //Creamos las conexiones entre el vertice y los vertices adyacentes
  if(par==1){
    if(*ant_j==0&&*ant_i==0){
      G->InsertaArista(G->GetVertice(id),G->GetVertice(999));
      G->InsertaArista(G->GetVertice(999),G->GetVertice(id));
      if(mat[*ant_j+1][*ant_i]!=0){
  G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
  G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i+1]!=0){
  G->InsertaArista(G->GetVertice(id),G->GetVertice(id+1));
  G->InsertaArista(G->GetVertice(id+1),G->GetVertice(id));
      }
    }
    else if(*ant_i==0){
      if(mat[*ant_j-1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-8));
    G->InsertaArista(G->GetVertice(id-8),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+1));
    G->InsertaArista(G->GetVertice(id+1),G->GetVertice(id));
      }
      if(mat[*ant_j+1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
    G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }

    }
    else if(*ant_j==0){
      G->InsertaArista(G->GetVertice(id),G->GetVertice(999));
      G->InsertaArista(G->GetVertice(999),G->GetVertice(id));
      if(mat[*ant_j+1][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+7));
    G->InsertaArista(G->GetVertice(id+7),G->GetVertice(id));
      }
      if(mat[*ant_j+1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
    G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-1));
    G->InsertaArista(G->GetVertice(id-1),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+1));
    G->InsertaArista(G->GetVertice(id+1),G->GetVertice(id));
      }

    }
    else if(*ant_i==7 && *ant_j==0){
      if(mat[*ant_j+1][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+7));
    G->InsertaArista(G->GetVertice(id+7),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-1));
    G->InsertaArista(G->GetVertice(id-1),G->GetVertice(id));
      }
      if(mat[*ant_j+1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
    G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }


    }
    else if(*ant_i==7){
      if(mat[*ant_j+1][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+7));
    G->InsertaArista(G->GetVertice(id+7),G->GetVertice(id));
      }
      if(mat[*ant_j+1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
    G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-1));
    G->InsertaArista(G->GetVertice(id-1),G->GetVertice(id));
      }
      if(mat[*ant_j-1][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-9));
    G->InsertaArista(G->GetVertice(id-9),G->GetVertice(id));
      }
      if(mat[*ant_j-1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-8));
    G->InsertaArista(G->GetVertice(id-8),G->GetVertice(id));
      }
    }
    else{
      if(mat[*ant_j+1][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+7));
    G->InsertaArista(G->GetVertice(id+7),G->GetVertice(id));
      }
      if(mat[*ant_j+1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
    G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-1));
    G->InsertaArista(G->GetVertice(id-1),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+1));
    G->InsertaArista(G->GetVertice(id+1),G->GetVertice(id));
      }
      if(mat[*ant_j-1][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-9));
    G->InsertaArista(G->GetVertice(id-9),G->GetVertice(id));
      }
      if(mat[*ant_j-1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-8));
    G->InsertaArista(G->GetVertice(id-8),G->GetVertice(id));
      }


    }
  }
  else{
    if(*ant_i==0){

      if(mat[*ant_j+1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
    G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+1));
    G->InsertaArista(G->GetVertice(id+1),G->GetVertice(id));
      }
 
      if(mat[*ant_j-1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-8));
    G->InsertaArista(G->GetVertice(id-8),G->GetVertice(id));
      }
        if(mat[*ant_j+1][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+9));
    G->InsertaArista(G->GetVertice(id+9),G->GetVertice(id));
      }
        if(mat[*ant_j-1][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-7));
    G->InsertaArista(G->GetVertice(id-7),G->GetVertice(id));
      }
    }
    else if(*ant_i==7){

      if(mat[*ant_j+1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
    G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-1));
    G->InsertaArista(G->GetVertice(id-1),G->GetVertice(id));
      }
      if(mat[*ant_j-1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-8));
    G->InsertaArista(G->GetVertice(id-8),G->GetVertice(id));
      }

    }
    else{
      if(mat[*ant_j+1][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+9));
    G->InsertaArista(G->GetVertice(id+9),G->GetVertice(id));
      }
      if(mat[*ant_j+1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+8));
    G->InsertaArista(G->GetVertice(id+8),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i-1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-1));
    G->InsertaArista(G->GetVertice(id-1),G->GetVertice(id));
      }
      if(mat[*ant_j][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id+1));
    G->InsertaArista(G->GetVertice(id+1),G->GetVertice(id));
      }
      if(mat[*ant_j-1][*ant_i+1]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-7));
    G->InsertaArista(G->GetVertice(id-7),G->GetVertice(id));
      }
      if(mat[*ant_j-1][*ant_i]!=0){
    G->InsertaArista(G->GetVertice(id),G->GetVertice(id-8));
    G->InsertaArista(G->GetVertice(id-8),G->GetVertice(id));
      }
    }
  }

}

void Verificar_Eliminacion(int id,   list<Vertice*> *lista){
  Vertice *aux;
  cout<<"Recorrido por Color:"<<endl;
  lista=G->RecorridoPorColor(G->GetVertice(id));
  cout<<endl;
  if(lista->size()>=3){
    while(!lista->empty()){
      aux=lista->back();
      lista->pop_back();
      sonido=1;
    al_play_sample(burble, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);
    
      G->EliminarVertice(aux);
      EliminarP_Matriz(G->GetId(aux));
      puntaje+=10;
    }
  }
  lista->clear();
  delete lista;
  Verificar_Techo(lista);
}
void EliminarP_Matriz(int id){
  for(int i=0; i<14; i++){
    for(int j=0; j<8; j++){
      if(j+(8*i)==id){
	mat[i][j]=0;
      }
    }
  }
}

void Verificar_Techo( list<Vertice*> *lista){
  int bandera=0;
  for(int i=0; i<14; i++){
    for(int j=0; j<8; j++){
      bandera=0;
      if(mat[i][j]!=0){
	cout<<"RecorridoProfunidad:"<<endl;
	lista=G->RecorridoProfundidad(G->GetVertice(j+(8*i)));
	cout<<endl;
	while(!lista->empty()){
	  if(G->GetId(lista->back())==999){
	    bandera=1;
	  }
	  lista->pop_back();

	}
	if(bandera==0){
    sonido=1;
	  al_play_sample(burble, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);

    G->EliminarVertice(G->GetVertice(j+(8*i)));
	  EliminarP_Matriz(j+(8*i));
	  nuevo_puntaje+=20;
	}
	delete lista;
      }
    }
  }
}
void Verificar_Matriz(){
  int band=0;
  for(int i=0; i<14; i++){
    for(int j=0; j<8; j++){
      if(mat[i][j]!=0){
	if(i>10-techo)
	  lost=1;
	band=1;
      }
    }
  }
  if(band==0){
    NIVEL_SIG++;
  }

}

void Cascaron(){
Jugador blanco={"\0",0};
Info.open("Resultados.dat", ios::in|ios::binary);
int i=0;
if(!Info){//Si no existe el archivo Resultados.dat entonces lo crea
  Info.open("Resultados.dat",ios::out  | ios::binary);
  if(!Info){//Comprueba que se halla abierto correctamente
    cerr<<endl<<"Error abriendo archivo de Resultados\n";
  }
  else{
    Info.seekg(0, ios::beg);//Se recorre hasta el inicio del archivo
    for(i=0; i<1000 ; i++){
      //Info.seekp(i*sizeof(Jugador), ios::beg);
      Info.write((char *)(&blanco),sizeof(Jugador));
    }
    Info.close();
  }
 }
 else{
   cout<<"Cascaron no creado, archivo binario ya existe\n";
   Info.close();
 }
}
void Consulta(){
  cout<<"Entre a consulta\n";
  Jugador aux;
  int find=0, i=0;
  Info.open("Resultados.dat", ios::in | ios::out | ios::binary);
  if (!Info){
    cerr<<endl<<"Error abriendo archivo de Resultados\n";
    return;
  }
  for(i=0; i<1000; i++){
    Info.seekg(i*sizeof(Jugador), ios::beg);
    Info.read((char *)(&aux), sizeof(Jugador));
    if(!strcmp(Alias, aux.alias)){
      find=1;
      pos=i;
      break;
    }
    else if(!strcmp("\0", aux.alias)){
      find=0;
      break;
    }
  }
  if(!find){
    Info.seekp(i*sizeof(Jugador), ios::beg);
    strcpy(aux.alias,Alias);
    aux.puntaje=0;
    Info.write((char*)(&aux), sizeof(Jugador));
    pos=i;
  }
  puntaje=aux.puntaje;
  cout<<"La posicion es "<<pos<<" y tiene un puntaje de "<<aux.puntaje<<endl;
  Info.close();
}

void Guardar(){
  Jugador aux;
  Info.open("Resultados.dat", ios::in | ios::out  | ios::binary);
  if (!Info){
    cerr<<endl<<"Error abriendo archivo de Resultados\n";
    return;
  }
  Info.seekg(pos*sizeof(Jugador), ios::beg);
  Info.read((char *)(&aux), sizeof(Jugador));
  cout<<"Score Guardado: ";
  strcpy(aux.alias,Alias);
  aux.puntaje=puntaje;
  cout<<aux.alias<<"  "<<aux.puntaje<<"\n";
  Info.seekp(pos*sizeof(Jugador), ios::beg);
  Info.write((char*)(&aux), sizeof(Jugador));
  //cout<<"Guardado :v!!!!!!!!!!!!!!!\n\n\n\n\n";
  Info.close();
}
void Sort(){
 Info.open("Resultados.dat",ios::binary|ios::in|ios::out);
 if (!Info){
   cerr<<endl<<"Error abriendo archivo de Resultados\n";
   return;
 }
 int i=0,may;
 Jugador obj1,obj2,temp;
 for(i=0;i<1000-1;i++){
   may=i;
   for(int j=i+1;j<1000;j++){
     Info.seekg(may*sizeof(Jugador),ios::beg);
     Info.read((char*)&obj1,sizeof(obj1));
     Info.seekg(j*sizeof(Jugador),ios::beg);
     Info.read((char*)&obj2,sizeof(obj2));
     if(obj2.puntaje>obj1.puntaje){
       may=j;
     }
   }
   Info.seekg(i*sizeof(Jugador),ios::beg);
   Info.read((char*)&temp,sizeof(temp));
   Info.seekp(may*sizeof(Jugador),ios::beg);
   Info.read((char*)&obj1,sizeof(obj1));
   Info.seekp(i*sizeof(Jugador),ios::beg);
   Info.write((char*)&obj1,sizeof(temp));
   Info.seekp(may*sizeof(Jugador),ios::beg);
   Info.write((char*)&temp,sizeof(obj1));
 }
 Info.close();
 Imprimir();
}

void Imprimir(){
  Info.open("Resultados.dat",ios::binary|ios::in|ios::out);
  if (!Info){
    cerr<<endl<<"Error abriendo archivo de Resultados\n";
    return;
  }
  int i=0;
  Jugador aux;
  cout<<endl<<"========================================================================="<<endl;
  cout<<"=================================HIGHSCORES=============================="<<endl;
  cout<<"========================================================================="<<endl;
  for(i=0;i<1000;++i){
    Info.seekg(i*sizeof(Jugador), ios::beg);
    Info.read((char *)(&aux), sizeof(Jugador));
    if(!strcmp(aux.alias,"\0")){
      cout<<"========================================================================="<<endl;
      cout<<"===============================FIN HIGHSCORES============================"<<endl;
      cout<<"========================================================================="<<endl;
      break;
    }
    cout<<endl<<"Alias: "<<aux.alias<<" -  Puntaje: "<<aux.puntaje<<endl;
  }
  Info.close();

}

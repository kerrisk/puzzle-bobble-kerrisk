#ifndef GRAFO_H
#define GRAFO_H
#include <string>
#include <iostream>
#include <queue>
#include <list>
#include <stack>

//colores: 1=azul, 2=cafe, 3=morada, 4=roja, 5=verde.
using namespace std;

class Arista;

class Vertice{
	Vertice *sig;
	Arista *ady;
	int id;
	int color;

	friend class Grafo;
};

class Arista{
	Arista *sig;
	Vertice *ady;
	friend class Grafo;
};

class Grafo{
	Vertice *h;
	public:
		void Inicializa();
		bool Vacio();
		int Tamano();
		Vertice *GetVertice(int id);
		int GetColor(Vertice *vert);
		int GetId(Vertice *vert);
		void SetColor(Vertice *vert, int);
		void InsertaArista(Vertice *origen, Vertice *destino);
		void InsertaVertice(int id, int color);
		void ListaAdyacencia();
		void EliminarArista(Vertice *origen, Vertice *destino);
		void Anular();
		void EliminarVertice(Vertice *vert);
		void Verificar(Vertice *vert);
		list<Vertice *> * RecorridoProfundidad(Vertice *vert);
		list<Vertice *> * RecorridoPorColor(Vertice *vert);

};


#endif

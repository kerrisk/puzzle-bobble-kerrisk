class Nivel{
 private:

  double tiempoDA;
 protected:
    struct jugador{
    char *name;
    double puntaje;
  };

 public:

  Nivel(){}
  virtual ~Nivel(){}
  virtual void Cargar()=0;

  void Precargar_Matriz(const char* nombre){
    if(mat!=NULL){
        for(int i=0; i<14 ; i++){
          free(mat[i]);
        }
        free(mat);
        mat= (int **) calloc(14, sizeof(int *));
        for(int i=0; i<14 ; i++){
          mat[i]=(int*) calloc(8, sizeof(int*));
        }
    }
    for(int i=0; i<14 ; i++){
      for(int j=0; j<8 ;j++){
	mat[i][j]=0;
      }
    }
    if(G!=NULL){
      if(!G->Vacio())
	G->Anular();
      G->Inicializa();
      G->InsertaVertice(999,0);
    }
    int j=0,i=0;
    int par=0, color;
    tiempoDisparoAuto=getTiempoDA();
    Archivo=fopen(nombre,"r");
    if (Archivo==NULL){
      fprintf(stderr, "failed to load level %i!\n",NIVEL_ACTUAL);
      Destruir();
      exit(-1);
    }
    fscanf(Archivo,"%i",&color);
    while(!feof(Archivo)){
      if (!color){
       mat[j][i]=0;
        i++;
	if(i>6){
	  i=0;
	  j+=1;
	  cout<<endl;
	}
	cout<<color<<" ";
	fscanf(Archivo,"%i",&color);
      continue;
      }
      if(i>6){
        i=0;
      	j+=1;
      	cout<<endl;
      }
      cout<<color<<" ";
     
      
      if(j%2==0){
        par=1;
      }
      else{
        par=0;
      }
      mat[j][i]=1;
      G->InsertaVertice(i+(8*j), color);
      Conectar(&i, &j, i+(8*j), par);
      i+=1;
      fscanf(Archivo,"%i",&color);
    }
    fclose(Archivo);
    cout<<endl;
  }
  inline void setTiempoDA(double t){tiempoDisparoAuto=t;}
  inline double getTiempoDA(){return tiempoDisparoAuto;}
};
class Nivel1: public Nivel{
  void Cargar(){
    Precargar_Matriz("Nivel1.txt");
    setTiempoDA(10);
    tiempo=0;
    shot=0;
  }
};
class Nivel2: public Nivel{
  void Cargar(){
    Precargar_Matriz("Nivel2.txt");
    setTiempoDA(5);
    tiempo=0;
    shot=0;
  }
};
class Nivel3: public Nivel{
  void Cargar(){
    Precargar_Matriz("Nivel3.txt");
    setTiempoDA(10);
    tiempo=0;
    shot=0;
  }
};


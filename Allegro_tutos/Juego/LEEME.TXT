Requerimientos del sistema 
   Para poder compilar y ejecutar el juego necesitas tener: 
       Mac OS o Fedora. 
       Instalar allegro 
       Y compilar el juego 

Instalar Allegro 

	 a)Mac OS 

	 a.1) Necesitas tener Homebrew, que es un gestor de paquetes para macOS. Para instalarlo debes ejecutar el síguete comando “/usr/bin/ruby -e "$(curl-fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)	" ” 
	 a.2) Luego para instalar allegro ejecutar “brew install allegro” 
	 a.3) Para compilar el proyecto solo abrir una teminal en la carpeta raíz del Juego y ejecutar el comando “make” 

	 b)GNU/Linux (Fedora | CentOS) 

	 b.1)Para instalar Allegro debes ejecutar el siguiente comando “sudo dnf install allegro” 
	 b.2)Para compilar el proyecto solo abrir una teminal en la carpeta raíz del Juego y ejecutar el comando “make” 
